package ga101;

public class Population {
	
	Individual[] individuals;
	
	public Population(int populationSize, boolean initialize) {
		this.individuals = new Individual[populationSize];
		
		if(initialize) {
			for(int i = 0; i < this.size(); i++) {
				this.individuals[i] = new Individual();
			}
		}
	}
	
	public Individual getFittest() {
		Individual fittest = this.individuals[0];
		
		for (int i = 0; i < this.size(); i++) {
			if(fittest.getFitness() < this.individuals[i].getFitness()) {
				fittest = this.individuals[i];
			}
		}
		
		return fittest;
	}
	
	public void saveIndividula(int index, Individual i) {
		this.individuals[index] = i;
	}
	
	public Individual getIndividual(int i) {
		return this.individuals[i];
	}
	
	public int size() {
		return this.individuals.length;
	}
	
}
