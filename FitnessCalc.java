package ga101;

public class FitnessCalc {
	
	static byte[] solution = new byte[64];
	
	public static void setSolution(byte[] s) {
		solution = s;
	}
	
	public static void setSolution(String sol) {
		solution = new byte[sol.length()];
		
		for (int i = 0; i < sol.length(); i++) {
			String ch = sol.substring(i, i+1);
			if(ch.contains("0") || ch.contains("1")) {
				solution[i] = Byte.parseByte(ch);
			} else {
				solution[i] = 0;
			}
		}
	}
	
	static int getFitness(Individual individual) {
		int fitness = 0;
		
		for(int i = 0; i < individual.size() && i < solution.length; i++) {
			if(individual.getGene(i) == solution[i]) {
				fitness++;
			}
		}
		
		return fitness;
	}
	
	static int getMaxFitness() {
		return solution.length;
	}
	
}
