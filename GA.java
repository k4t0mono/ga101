package ga101;

import java.util.Scanner;

public class GA {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Insert an 8 bytes hex number: ");
		String hex = scan.nextLine();
		
		System.out.print("Population size: ");
		int size = scan.nextInt();
		
		System.out.print("Crossover rate: ");
		double cr = scan.nextDouble();
		
		System.out.print("Mutation rate: ");
		double mr = scan.nextDouble();
		
		System.out.print("Tournament size: ");
		int ts = scan.nextInt();
		
		System.out.print("Elitsm: ");
		boolean e = scan.nextBoolean();
		
		System.out.println("");
		scan.close();
		
		hex = hex.replaceAll("\\s+","");
		Long num = Long.parseUnsignedLong(hex, 16);
		
		FitnessCalc.setSolution(Long.toBinaryString(num));
		Algorithm.setValues(cr, mr, ts, e);
		
		Population pop = new Population(size, true);
		
		int generation = 0;
		while(pop.getFittest().getFitness() < FitnessCalc.getMaxFitness()) {
			generation++;
			
			Individual fittest = pop.getFittest();
			Long g = Long.parseUnsignedLong(fittest.toString(), 2);
			
			System.out.println("Generation: " + generation + "\t Fittest: " + fittest.getFitness()
					+ "\t" + String.format("0x%016X", g));
			pop = Algorithm.evolvePopulation(pop);
		}
		
		System.out.println("Solution found!");
		System.out.println("Generation: " + generation);
		System.out.println("Genes: " + String.format("0x%16X", num));
		System.out.println(pop.getFittest());
	}

}
