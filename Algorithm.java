package ga101;

public class Algorithm {
	
	private static double uniformRate = 0.5;
	private static double mutationRate = 0.015;
	private static int tournamentSize = 5;
	private static boolean elitism = true;
	
	public static void setValues(double cr, double mr, int ts, boolean e) {
		uniformRate = cr;
		mutationRate = mr;
		tournamentSize = ts;
		elitism = e;
	}
	
	public static Population evolvePopulation(Population pop) {
		Population newPop = new Population(pop.size(), false);
		
		if(elitism) {
			newPop.saveIndividula(0, pop.getFittest());
		}
		
		int elitismOffset = 0;
		if(elitism) {
			elitismOffset = 1;
		}
		
		// Crossover
		for(int i = elitismOffset; i < pop.size(); i++) {
			Individual a = tournamentSelection(pop);
			Individual b = tournamentSelection(pop);
			
			newPop.saveIndividula(i, crossover(a, b));
		}
		
		// Mutations
		for (int i = elitismOffset; i < newPop.size(); i++) {
			mutate(newPop.getIndividual(i));
		}
		
		return newPop;
	}

	public static Individual crossover(Individual a, Individual b) {
		Individual newInd = new Individual();
		
		for (int i = 0; i < a.size(); i++) {
			if(Math.random() <= uniformRate) {
				newInd.setGene(i, a.getGene(i));
			} else {
				newInd.setGene(i, b.getGene(i));
			}
		}
		
		return newInd;
	}
	
	private static void mutate(Individual individual) {
		for (int i = 0; i < individual.size(); i++) {
			if(Math.random() <= mutationRate) {
				individual.setGene(i, (byte) Math.round(Math.random()));
			}
		}
	}

	private static Individual tournamentSelection(Population pop) {
		Population tournament = new Population(tournamentSize, false);
		
		for (int i = 0; i < tournamentSize; i++) {
			int randomId = (int) (Math.random() * pop.size());
			tournament.saveIndividula(i, pop.getIndividual(randomId));
		}
		
		return tournament.getFittest();
	}
	
}
