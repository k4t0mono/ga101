package ga101;

public class Individual {
	
	static int defaultGeneLength = 64;
	private byte genes[] = new byte[defaultGeneLength];
	private int fitness = 0;
	
	public void generateIndividual() {
		for (int i = 0; i < this.size(); i++) {
			// Gerar os genes aleatoriamente
			this.genes[i] = (byte) Math.round(Math.random());
		}
	}
	
	public static void setDefaultGeneLength(int s) {
		defaultGeneLength = s;
	}
	
	public byte getGene(int i) {
		return this.genes[i];
	}
	
	public void setGene(int i, byte value) {
		this.genes[i] = value;
		this.fitness = 0;
	}
	
	public int size() {
		return defaultGeneLength;
	}
	
	public int getFitness() {
		if(fitness == 0) {
			fitness = FitnessCalc.getFitness(this);
		}
		
		return this.fitness;
	}

	@Override
	public String toString() {
		String geneString = "";
		for (int i = 0; i < this.size(); i++) {
			geneString += getGene(i);
		}
		
		return geneString;
	}

}
